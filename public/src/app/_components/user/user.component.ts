import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';


export interface UserElement {
  position: number;
  username: string;
  email: string;
}

const ELEMENT_DATA: UserElement[] = [
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
  {position: 1, username: 'Cristian', email: 'cristian@gmail'},
  {position: 2, username: 'Cacau', email: 'cristian@gmail'},
  {position: 3, username: 'Pedro', email: 'cristian@gmail'},
];


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  displayedColumns: string[] = ['position', 'username', 'email', 'acao'];
  dataSource = new MatTableDataSource<UserElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

}
