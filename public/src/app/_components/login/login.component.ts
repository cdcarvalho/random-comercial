import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/_service/user/user.service';
import { AlertService } from 'src/app/_service/alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  User;
  returnUrl: string;

  constructor(public userService: UserService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router) {
    this.User = new User();
  }

  async ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
  }

  async login() {
    try {
      const username = await this.userService.login(this.User);
      this.router.navigate(['/home']);
      this.alertService.info("Seja Bem Vindo! " + username);
    } catch (error) {
      this.alertService.error(error.message);
    }
  }
}
