import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/_service/alert/alert.service';
import { UserService } from 'src/app/_service/user/user.service';

@Component({
  selector: 'register-component',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  currentUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {
    this.userService.currentUser.subscribe(x => this.currentUser = x);
    
    if (this.userService.currentUserValue) {
      this.router.navigate(['/register']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  async register() {

    try {
      this.submitted = true;

      if (this.registerForm.invalid) {
        return;
      }

      this.loading = true;
      await this.userService.register(this.registerForm.value);
      this.router.navigate(['/login']);
      this.alertService.success("Cadastro efetuado com suceso!");

    } catch (error) {
      this.alertService.error(error.message);
    }
  }
}
