import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_service/user/user.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  mensagem;
  currentUser: User;

  constructor(private userService : UserService) { 
    this.userService.currentUser.subscribe(x => this.currentUser = x);
    
  }

  ngOnInit() {

  }

}
