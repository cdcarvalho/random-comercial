import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import { UserService } from 'src/app/_service/user/user.service';
import { User } from 'src/app/model/user';
import { AlertService } from 'src/app/_service/alert/alert.service';
import { Router } from '@angular/router';

declare const Parse: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser: User;
  public logado : Boolean;
  
  constructor(
      public userService : UserService,
      private alertService: AlertService,
      private router: Router
  ) {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.serverURL = environment.serverURL;
    Parse.liveQueryServerURL = environment.liveQueryServerURL;

    this.userService.currentUser.subscribe(x => this.currentUser = x);
    this.router.navigate(['/login']);
  }

  
  async logout() {
    try {
      await this.userService.logout();
      this.router.navigate(['/login']);
      this.alertService.info("Logout efetuado com sucesso!");
    } catch (error) {
      this.alertService.error(error.message);
    }
  }

  ngOnInit() { }
}

