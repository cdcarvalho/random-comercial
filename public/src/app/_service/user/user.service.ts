import { Injectable } from '@angular/core';
import { User } from 'src/app/model/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { __await } from 'tslib';

declare const Parse: any;

export class UserService {
  User;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor() {
    this.User = Parse.Object.extend('User');
    this.User = new User();

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  async login(user: User) {

    const userParse = await Parse.User
      .logIn(user.username, user.password).then(function (user) {
        return user;
      }).catch(function (error) {
        throw Error(error);
      });

    localStorage.setItem('currentUser', JSON.stringify(userParse.id));
    this.currentUserSubject.next(userParse.id);

    return userParse.attributes.username;
  }

  async register(new_user: any) {

    var user = new Parse.User();
    user.set("username", new_user.username);
    user.set("password", new_user.password);
    user.set("email", new_user.email);

    try {
      return await user.signUp();
    } catch (error) {
      throw Error(error);
    };
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  async logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
